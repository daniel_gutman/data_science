#!/usr/bin/env python3

import datetime as dt
import pandas as pd
import numpy as np
import os

class Parameters:
    def __init__(self):
        self.csvInput = os.path.join(getAppPath(),'hotels_data.csv')
        self.csvOutput = os.path.join(getAppPath(),'Hotels_data_Changed.csv')
        self.csvInputDf = readCsv(self.csvInput)
        self.csvOutputDf = readCsv(self.csvOutput)


def getAppPath():
    return os.path.dirname(os.path.realpath(__file__))


def readCsv(path):
    df = pd.read_csv(path,encoding="utf8")
    return df


def writeCsv(df,csv):
    df.to_csv(csv)


def dayDiff(df):
    df['Snapshot Date'] = pd.to_datetime(df['Snapshot Date'])
    df['Checkin Date'] = pd.to_datetime(df['Checkin Date'])
    df['Day Diff'] = (df['Checkin Date'] - df['Snapshot Date']) / np.timedelta64(1, 'D')


def weekDay(df):
    df['Week Day'] = df['Checkin Date'].dt.dayofweek
    # df['Week Day'] = df['Week Day'].map({0: 'Mon', 1: 'Tue', 2: 'Wed', 3: 'Thu', 4: 'Fri',5: 'Sat',6 : 'Sun'})


def discountDiff(df):
    df['Discount Diff'] = (df['Original Price'] - df['Discount Price']).astype(str)


def DiscountPerc(df):
    df['Discount Perc'] = ((df['Discount Price'] / df['Original Price']) * 100).round(2)


if __name__ == '__main__':
        par = Parameters()

        df = par.csvInputDf
        dayDiff(df)
        weekDay(df)
        discountDiff(df)
        DiscountPerc(df)
        writeCsv(df,par.csvOutput)
