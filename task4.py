#!/usr/bin/env python3

from pyspark.sql import SparkSession
from pyspark.sql.functions import concat, col, lit
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.clustering import BisectingKMeans

# SparkSession
spark = SparkSession.builder \
     .master("local") \
     .appName("Clustering") \
     .getOrCreate()
spark.sparkContext.setLogLevel("ERROR")

def get150Hotels(data):

    hotels = data.groupBy('Hotel Name').count().orderBy('count', ascending=False).select('Hotel Name').limit(150)
    newData = data.join(hotels, data['Hotel Name'] == hotels['Hotel Name']).drop(hotels['Hotel Name'])

    return newData

def get40Checkin(data):

    checkIn = data.groupBy('Checkin Date').count().orderBy('count', ascending=False).select('Checkin Date').limit(40)
    newData = data.join(checkIn, data['Checkin Date'] == checkIn['Checkin Date']).drop(checkIn['Checkin Date'])

    return newData

def get160NormailizeNumbers(data):

    data = data.withColumn('Discount Price', data['Discount Price'].cast('int')).groupBy('Hotel Name', 'Checkin Date', 'Discount Code').min('Discount Price').withColumnRenamed(
        'min(Discount Price)', 'Discount Price')

    data = data.withColumn('Checkin Date & Discount Code',
                           concat(data['Checkin Date'], lit(':'), data['Discount Code'])).orderBy('Checkin Date & Discount Code')

    maxPrice = data.agg({"Discount Price": "max"}).collect()[0][0]

    data = data.withColumn('Discount Price', (data['Discount Price'] / maxPrice) * 100).groupBy('Hotel Name').pivot('Checkin Date & Discount Code').min('Discount Price')

    newData = data.na.fill(-1)

    return newData

def readCsv():
    df = spark.read \
        .option("header", "true") \
        .option("inferSchema", "true") \
        .csv('Clustering.csv')
    return df


def clustering(df):

    noFeature = ['Hotel Name']

    result = VectorAssembler(
        inputCols=[col for col in df.columns if col not in noFeature],
        outputCol='features')

    # transform the original dataframe to another one with features column using VectorAssembles
    df_with_features = result.transform(df)

    print('''Checking the K from 2 to 10 of clusters by using "Within Set Sum of Squared Errors" (WSSSE)''')
    for k in range(2, 10):
        bkm = BisectingKMeans(k=k, featuresCol='features')
        model = bkm.fit(df_with_features)
        wssse = model.computeCost(df_with_features)
        print("K={}".format(k))
        print("WSSE = " + str(wssse))
        print('**' * 30)

    # The best case is when k=4 because in this case the WSSSE is at the minimum
    bkm4 = BisectingKMeans(k=4, featuresCol='features')
    model_k4 = bkm4.fit(df_with_features)

    transformed = model_k4.transform(df_with_features).select("Hotel Name", "prediction")
    print('Groups:')
    print(transformed.groupBy('prediction').count().show())



if __name__ == '__main__':

    data = spark.read.option("header", "true").csv('Hotels_Data_Changed.csv')
    data = data.select('Hotel Name', 'Checkin Date', 'Discount Code', 'Discount Price')

    newData = get150Hotels(data)

    newData = get40Checkin(newData)

    newData = get160NormailizeNumbers(newData)

    newData.coalesce(1).write.csv('Clustering')
    newData.toPandas().to_csv('Clustering.csv', index=False)

    df = readCsv()

    clustering(df)

